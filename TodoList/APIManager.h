//
//  APIManager.h
//  TodoList
//
//  Created by Комп диза on 12/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIManager : NSObject

+ (void)tasksWithHandler:(void (^)(NSArray *result, NSError *error))completionHandler;

@end
