//
//  APIManager.m
//  TodoList
//
//  Created by Комп диза on 12/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import "APIManager.h"
#import <AFNetworking/AFNetworking.h>


@implementation APIManager

NSString *const taskURL = @"http://jsonplaceholder.typicode.com/todos";

+ (void)tasksWithHandler:(void (^)(NSArray *result, NSError *error))completionHandler
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString: taskURL];
    NSURLRequest *request = [NSURLRequest requestWithURL: URL];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error)
    {
        if (error)
        {
            NSLog(@"%@", error);
            completionHandler(nil, error);
        }
        else
        {
            completionHandler(responseObject, error);
        }
    }];
    [dataTask resume];
}

@end
