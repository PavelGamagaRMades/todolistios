//
//  Task+CoreDataProperties.m
//  TodoList
//
//  Created by Комп диза on 12/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Task+CoreDataProperties.h"

@implementation Task (CoreDataProperties)

@dynamic isDone;
@dynamic text;

@end
