//
//  Task.h
//  TodoList
//
//  Created by Комп диза on 12/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ITask.h"

NS_ASSUME_NONNULL_BEGIN

@interface Task : NSManagedObject <ITask>
@property (strong,nonatomic) NSNumber *isDone;
@property (strong,nonatomic) NSString *text;
@end

NS_ASSUME_NONNULL_END

#import "Task+CoreDataProperties.h"
