//
//  Task.m
//  TodoList
//
//  Created by Комп диза on 12/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import "Task.h"

@implementation Task

@dynamic text;
@dynamic isDone;

- (id) init
{
    return [self initWithText: @""];
}

- (id)initWithText:(NSString *)text
{
    self = [super init];
    if (self != nil)
    {
        self.text = text;
        self.isDone = @NO;
    }
    return self;
}

@end
