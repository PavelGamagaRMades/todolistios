//
//  TaskCell.h
//  TodoList
//
//  Created by Комп диза on 05/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITask.h"

@protocol TaskCellDelegate <NSObject>

- (void)taskWillDeleted:(id<ITask>) task;
- (void)taskWillChanged:(id<ITask>) task;

@end

@interface TaskCell : UITableViewCell

@property (strong, nonatomic) id<ITask> task;
@property (weak, nonatomic) id<TaskCellDelegate>  delegate;
@property (weak, nonatomic) IBOutlet UISwitch *doneSwitch;
@property (weak, nonatomic) IBOutlet UILabel *taskTextLabel;

- (IBAction)taskDonePressed:(id)sender;
- (IBAction)taskRemovePressed:(id)sender;
- (void)configureWithTask:(id<ITask>)task;

@end
