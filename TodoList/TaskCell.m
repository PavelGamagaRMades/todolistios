//
//  TaskCell.m
//  TodoList
//
//  Created by Комп диза on 05/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import "TaskCell.h"

@interface TaskCell()
- (void) updateGUIWithTask;
@end


@implementation TaskCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

 - (void)configureWithTask:(id<ITask>)task
{
     self.task = task;
     [self updateGUIWithTask];
}


- (void)updateGUIWithTask
{
    if (self.task != nil)
    {
        self.doneSwitch.on = self.task.isDone.boolValue;
        if (self.task.isDone.boolValue)
        {
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString: self.task.text];
            [attributedString addAttribute:NSStrikethroughStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0,[attributedString length]}];
             self.taskTextLabel.attributedText = attributedString;
            [self.taskTextLabel setTextColor: [UIColor lightGrayColor]];
        }
        else
        {
            self.taskTextLabel.text = [self.task.text copy];
            [self.taskTextLabel setTextColor: [UIColor blackColor]];
        }
    }
}

- (IBAction)taskDonePressed:(id)sender
{
    if (self.task != nil)
    {
        self.task.isDone = [NSNumber numberWithBool: self.doneSwitch.on];
        [self updateGUIWithTask];
        [self.delegate taskWillChanged: self.task];
    }
}

- (IBAction)taskRemovePressed:(id)sender
{
    [self.delegate taskWillDeleted: self.task];
}
@end
