//
//  TaskList.h
//  TodoList
//
//  Created by Комп диза on 06/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//
@import CoreData;

#import <Foundation/Foundation.h>


#import "Task.h"

@interface TaskList : NSObject {
    NSMutableArray *taskArray;
    NSManagedObjectContext *context;
    
}

- (NSMutableArray*) getList;

- (void) addTaskWithText: (NSString*)text done: (BOOL)done;

- (void) removeTask: (Task*) task;

- (void) updateTask: (Task*)task text: (NSString*)text done: (BOOL) done;

- (void) update;

- (void) save; 

@end
