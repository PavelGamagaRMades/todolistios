//
//  TaskRepository.h
//  TodoList
//
//  Created by Комп диза on 07/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Task.h"

@interface TaskRepository : NSObject 

- (NSArray<id<ITask>> *)tasks;
- (void)removeTask:(id<ITask>)task;
- (void)clearTasks;
- (BOOL)saveContext;
- (id<ITask>)createTask;
- (id<ITask>)createTask:(NSDictionary *)dict;

@end
