//
//  TaskRepository.m
//  TodoList
//
//  Created by Комп диза on 07/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import "TaskRepository.h"
#import "AppDelegate.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Task+CoreDataProperties.h"

@implementation TaskRepository

- (id)init
{
    self = [super init];
    return self;
}

- (NSArray *)tasks
{
    return [Task MR_findAll];
}

- (id<ITask>)createTask
{
    return [Task MR_createEntity];
}

- (id<ITask>)createTask:(NSDictionary *)dict
{
    NSString *text = [dict objectForKey: @"title"];
    NSNumber *isDone = [dict objectForKey: @"completed"];
    
    if (text != nil && isDone != nil)
    {
        id<ITask> task = [self createTask];
        task.text = text;
        task.isDone = isDone;
        return task;
    }
    return nil;
}

- (void)removeTask:(Task *)task
{
    [task MR_deleteEntity];
}

- (void)clearTasks
{
    [Task MR_truncateAll];
}

- (BOOL)saveContext
{
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    if (context != nil)
    {
        if ([context hasChanges])
        {
            [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error)
            {
                if (success)
                {
                    NSLog(@"You successfully saved your context.");
                }
                else if (error)
                {
                    NSLog(@"Error saving context: %@", error.description);
                }
            }];
            return NO;
        }
    }
    return YES;
}
@end
