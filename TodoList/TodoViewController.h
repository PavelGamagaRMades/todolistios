//
//  TodoViewController.h
//  TodoList
//
//  Created by Комп диза on 04/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TaskCell.h"
#import "TaskRepository.h"


typedef NS_ENUM(NSInteger, ScreenMode) {
    Browse,
    Search
};

@interface TodoViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, TaskCellDelegate> 

@property (weak, nonatomic) IBOutlet UITableView *taskTable;
@property (weak, nonatomic) IBOutlet UITextField *createTaskField;
@property (weak, nonatomic) IBOutlet UILabel *taskCounter;
@property (weak, nonatomic) IBOutlet UISwitch *doneSwitch;
@property (weak, nonatomic) IBOutlet UIButton *findButton;

- (IBAction)clearCompletedPressed:(id)sender;
- (IBAction)createNewTaskPressed:(id)sender;
- (IBAction)doneTasksPressed:(id)sender;
- (IBAction)loadFromWebPressed:(id)sender;
- (IBAction)scenePressed:(id)sender;
- (IBAction)findPressed:(id)sender;
- (IBAction)createTaskFieldChanged:(id)sender;


@end
