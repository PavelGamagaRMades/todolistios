//
//  TodoViewController.m
//  TodoList
//
//  Created by Комп диза on 04/07/16.
//  Copyright © 2016 Комп диза. All rights reserved.
//

#import "TodoViewController.h"
#import "AppDelegate.h"
#import "APIManager.h"
#import "ITask.h"

@interface TodoViewController ()

@property (strong, nonatomic) NSMutableArray<id<ITask>> *taskArray;
@property (strong, nonatomic) NSMutableArray<id<ITask>> *showTaskArray;
@property (assign, nonatomic) ScreenMode currentMode;
@property (strong, nonatomic) TaskRepository *taskRepo;

- (void) setMode: (ScreenMode) mode;
- (void) updateScreen;
- (void) createTasksFromWeb: (NSArray*) webData;

@end

@implementation TodoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.taskRepo = [[TaskRepository alloc] init];
    self.taskArray = [NSMutableArray<id<ITask>> new];
    [self.taskArray addObjectsFromArray: [self.taskRepo tasks]];
    self.showTaskArray = self.taskArray;
    self.currentMode = Browse;
    [self updateScreen];

    [self.taskTable registerNib:[UINib nibWithNibName:NSStringFromClass([TaskCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([TaskCell class])];
    self.taskTable.dataSource = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clearCompletedPressed:(id)sender
{
    for(long i = self.taskArray.count - 1; i >= 0; i--)
    {
        id<ITask> task = self.taskArray[i];
        if(task.isDone.boolValue)
        {
            [self.taskRepo removeTask:task];
            [self.taskArray removeObjectAtIndex: i];
        }
    }
    [self.taskRepo saveContext];
    [self setMode: Browse];
    self.doneSwitch.on = false;
    [self updateScreen];
}

- (IBAction)createNewTaskPressed:(id)sender
{
    NSString *text = self.createTaskField.text;
    if (text != nil && text.length > 0)
    {
        id<ITask> task = [self.taskRepo createTask];
        task.text = text;
        [self.taskRepo saveContext];
        [self.taskArray addObject: task];
        [self.createTaskField resignFirstResponder];
        self.createTaskField.text = @"";
        [self setMode: Browse];
        [self updateScreen];
    }
}

- (IBAction)doneTasksPressed:(id)sender
{
    UISwitch *sw = (UISwitch*) sender;
    for(id<ITask> task in self.taskArray)
    {
        task.isDone = [NSNumber numberWithBool: sw.on];
    }
    [self.taskRepo saveContext];
    [self updateScreen];
}

- (IBAction)loadFromWebPressed:(id)sender
{
    [APIManager tasksWithHandler: ^(NSArray* data, NSError *error)
    {
        if (error)
        {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error" message:[NSString stringWithFormat:@"%@", error] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [self createTasksFromWeb: data];
        }
    }];
}


- (void)createTasksFromWeb:(NSArray *)webData
{
    [self setMode: Browse];
    [self.taskRepo clearTasks];
    [self.taskArray removeAllObjects];
    for(NSDictionary *obj in webData)
    {
        id<ITask> task = [self.taskRepo createTask: obj];
        if (task != nil)
        {
            [self.taskArray addObject: task];
        }
    }
    [self.taskRepo saveContext];
    [self updateScreen];
}

- (IBAction)scenePressed:(id)sender
{
    [self.createTaskField resignFirstResponder];
}

- (IBAction)findPressed:(id)sender
{
    if(self.currentMode == Browse)
    {
        NSString* mask = _createTaskField.text;
        if (mask == NULL || mask.length == 0)
        {
            return;
        }
        [self setMode: Search];
        for(id<ITask> task in self.taskArray)
        {
            if([task.text containsString: mask])
            {
                [self.showTaskArray addObject: task];
            }
        }
        [self.findButton setTitle: @"Clear" forState: UIControlStateNormal];
    }
    else
    {
        [self setMode: Browse];
        [self.findButton setTitle: @"Find" forState: UIControlStateNormal];
    }
    [self updateScreen];
}

-(void)setMode:(ScreenMode)mode
{
    if(self.currentMode == mode) return;
    self.currentMode = mode;
    if(self.currentMode == Search)
    {
        if (self.showTaskArray != self.taskArray)
        {
            [self.showTaskArray removeAllObjects];
        }
        else
        {
            self.showTaskArray = [[NSMutableArray alloc] initWithCapacity:20];
        }
    }
    else
    {
        [self.showTaskArray removeAllObjects];
        self.showTaskArray = self.taskArray;
    }
    
}

- (IBAction)createTaskFieldChanged:(id)sender
{
   
}

 - (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.showTaskArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id<ITask> task = self.showTaskArray [indexPath.row];
    NSString *cellId = NSStringFromClass([TaskCell class]);
    TaskCell *cell = [tableView dequeueReusableCellWithIdentifier: cellId];
    [cell configureWithTask: task];
    cell.delegate = self;
    return cell;
}


- (void)taskWillDeleted:(id<ITask>)task
{
    [self.taskRepo removeTask: task];
    [self.taskRepo saveContext];
    [self.taskArray removeObject: task];
    [self.showTaskArray removeObject: task];
    [self updateScreen];
}

- (void)taskWillChanged:(id<ITask>)task
{
    [self.taskRepo saveContext];
    [self updateScreen];
}

- (void) updateScreen
{
    int activeTaskCounter = 0;
    for(Task *task in self.taskArray)
    {
        if(!task.isDone.boolValue)
        {
            activeTaskCounter++;
        }
    }
    self.taskCounter.text = [NSString stringWithFormat:@"%0d items left", activeTaskCounter];
    [self.taskTable reloadData];
}

@end
